const express = require('express')

const router = express.Router()
const { name, version } = require('../package.json')
const { notFound } = require('./utils')
const user = require('./user')

router.get('/version', (_, res) => res.json({ name, version }))
router.use('/users', user)
router.all('*', notFound)

module.exports = router
