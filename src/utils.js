const notFound = (_, res) => res
  .status(404)
  .json({
    reason: 'Not found',
    message: 'These are not the pages you are looking for!',
  })

module.exports = { notFound }
