const jwt = require('jsonwebtoken')

const SECRET = process.env.SECRET || 'cupcake'
const User = require('./model')

const auth = async (req, res, next) => {
  try {
    const token = req.header('Authorization').replace('Bearer ', '')
    const data = jwt.verify(token, SECRET)
    const user = await User.findOne({ _id: data._id, 'tokens.token': token })

    if (!user) { throw new Error() }

    req.user = user
    req.token = token
    next()
  } catch (error) {
    res
      .status(401)
      .json({ reason: 'unauthorized' })
      .end()
  }
}
module.exports = auth
